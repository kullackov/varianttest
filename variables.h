#ifndef VARIABLES_H
#define VARIABLES_H

#include <values.h>
#include <vector>

//! if NOT_USE_VARIANT, then StringVariable receives only StringValue (no ValuesVariant)
//#define NOT_USE_VARIANT

class AbstractVariable {
public:
    virtual void addData(const ValuesVariant &newData) =0;
    virtual ~AbstractVariable() {}
};

#ifdef NOT_USE_VARIANT
class StringVariable : public AbstractVariable {
public:
    void addData(const StringValue &newData) {
        data.push_back(newData);
    }
private:
    virtual void addData(const ValuesVariant &newData) override {
        assert(mpark::holds_alternative<StringValue>(newData));
        addData(static_cast<const StringValue&>(mpark::get<StringValue>(newData).data));
    }
    std::vector<StringValue> data;
};
#else
class StringVariable : public AbstractVariable {
public:
    virtual void addData(const ValuesVariant &newData) override {
        assert(mpark::holds_alternative<StringValue>(newData) && "use #define NOT_USE_VARIANT to protect from type mismatch");
        data.push_back(mpark::get<StringValue>(newData).data);
    }
    std::vector<StringValue> data;
};
#endif

class NumberVariable : public AbstractVariable {
public:
    virtual void addData(const ValuesVariant &newData) override {
        assert(mpark::holds_alternative<NumberValue>(newData));
        data.push_back(mpark::get<NumberValue>(newData).data);
    }
    std::vector<NumberValue> data;
};

class DateTimeVariable : public AbstractVariable {
public:
    virtual void addData(const ValuesVariant &newData) override {
        assert(mpark::holds_alternative<DateTimeValue>(newData));
        data.push_back(mpark::get<DateTimeValue>(newData).data);
    }
    std::vector<DateTimeValue> data;
};

#endif // VARIABLES_H
