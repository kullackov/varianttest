#include <QVariant>
#include <QElapsedTimer>

#include <iostream>

#include <variables.h>
#include <helpers.h>

static int newCounter = 0;

void * operator new(size_t size)
{
    ++newCounter;
    void * p = malloc(size);
    return p;
}

void testMparkVariant()
{
    std::cout << "\nvoid testMparkVariant()\n";
    std::cout << "sizeof (DateTimeValue) = " << sizeof (DateTimeValue) << std::endl;
    std::cout << "sizeof (StringValue) = " << sizeof (StringValue) << std::endl;
    std::cout << "sizeof (NumberValue) = " << sizeof (NumberValue) << std::endl;
    std::cout << "sizeof (ValuesVariant) = " << sizeof (ValuesVariant) << " (size of max stored type + 1 byte)" << std::endl;

    std::vector<ValuesVariant> container = {StringValue("asd"), DateTimeValue(100), NumberValue(111)};

    StringVariable *stringVariable = new StringVariable();

    stringVariable->addData(StringValue("asd"));

    //! don't compile if defined NOT_USE_VARIANT
//    stringVariable->addData(container.at(0));
//    stringVariable->addData(DateTimeValue(44));

    AbstractVariable *abstractVariable = stringVariable;
    abstractVariable->addData(StringValue("asd"));

    //! throw exception if type different
//    abstractVariable->addData(DateTimeValue(44));
//    abstractVariable->addData(container.at(1));

    ValuesVariant variant;

    std::cout << "variant.index() == ERROR_TYPE is " << (variant.index() == 0) << std::endl;

    variant = DateTimeValue(100);

    std::cout << "variant.index() == DATE_TYPE is " << (variant.index() == DATE_TYPE) << std::endl;

    //! get/change value
    if (auto dateTimeValue = mpark::get_if<DateTimeValue>(&variant)) {
        ++dateTimeValue->data;
    }

    //! convert type, cope returned
    //! mpark::get throw exception if type different
    assert(mpark::holds_alternative<DateTimeValue>(variant));
    std::cout << "variant.data = " << mpark::get<DateTimeValue>(variant).data << std::endl;
    std::cout << "variant.isTime = " << mpark::get<DateTimeValue>(variant).isTime << std::endl;

    //! visit elements of container and process data based on type
    //! Case 1
    for (auto &variant : container) {
        if (const auto stringValue = mpark::get_if<StringValue>(&variant)) {
            std::cout << "stringValue->data = " << stringValue->data.toLatin1().constData() << std::endl;
        } else if (const auto numberValue = mpark::get_if<NumberValue>(&variant)) {
            std::cout << "numberValue->data = " << numberValue->data << std::endl;
            std::cout << "numberValue->isMissedValue = " << numberValue->isMissedValue << std::endl;
        } else if (const auto dateTimeValue = mpark::get_if<DateTimeValue>(&variant)) {
            std::cout << "dateTimeValue->data = " << dateTimeValue->data << std::endl;
            std::cout << "dateTimeValue->isTime = " << dateTimeValue->isTime << std::endl;
            std::cout << "dateTimeValue->isMissedValue = " << dateTimeValue->isMissedValue << std::endl;
        } else {
            std::cout << "error value" << std::endl;
        }
    }

    //! visit elements of container and process data based on type
    //! Case 2 (no difference except size of binary is smaller)
    for (auto &variant : container) {

        mpark::visit([](auto &&arg) {
            using T = std::decay_t<decltype(arg)>;

            //Ð²ÑÐ¿Ð¾Ð¼Ð¾Ð³Ð°ÑÐµÐ»ÑÐ½ÑÐµ ÐºÐ»Ð°ÑÑÑ Ð´Ð»Ñ Ð·Ð°Ð¼ÐµÐ½Ñ constexpr if (Ñ++17)
            static_if<std::is_same<StringValue, T>::value>([&](auto &&f) {
                std::cout << "data = " << f(arg).data.toLatin1().constData() << std::endl;
            });

            static_if<std::is_same<DateTimeValue, T>::value>([&](auto &&f) {
                std::cout << "data = " << f(arg).data << std::endl;
                std::cout << "isTime = " << f(arg).isTime << std::endl;
                std::cout << "isMissedValue = " << f(arg).isMissedValue << std::endl;
            });

            static_if<std::is_same<NumberValue, T>::value>([&](auto &&f) {
                std::cout << "data = " << f(arg).data << std::endl;
                std::cout << "isMissedValue = " << f(arg).isMissedValue << std::endl;
            });

        }, variant);
    }
}

Q_DECLARE_METATYPE(DateTimeValue);
Q_DECLARE_METATYPE(NumberValue);

void testQVariant()
{
    std::cout << "\nvoid testQVariant()\n";

    std::cout << "sizeof (DateTimeValue) = " << sizeof (DateTimeValue) << std::endl;
    std::cout << "sizeof (StringValue) = " << sizeof (StringValue) << std::endl;
    std::cout << "sizeof (NumberValue) = " << sizeof (NumberValue) << std::endl;
    std::cout << "sizeof (QVariant) = " << sizeof (QVariant) << " + dynamic memory for data" << std::endl;

    QVariant variant = QVariant::fromValue(DateTimeValue(100));

    //! Chaning value
    if (variant.canConvert<DateTimeValue>()) {
        auto numberValue = variant.value<DateTimeValue>();
        ++numberValue.data;
        variant = QVariant::fromValue(numberValue);
    }
}

static const qint64 ELEMENTS_COUNT = 10000000LL;

void testMparkVariantPerformace()
{
    std::cout << "\nvoid testMparkVariantPerformace()\n";

    QElapsedTimer t; t.start();

    newCounter = 0;

    std::vector<ValuesVariant> container; container.reserve(ELEMENTS_COUNT);

    for (qint64 i=0; i<ELEMENTS_COUNT; ++i) {
        if (i % 2) {
            container.push_back(DateTimeValue(111));
        } else {
            container.push_back(NumberValue(100));
        }
    }

    //! to measure app size
//    int wait;
//    std::cin >> wait;

    std::cout << "testMparkVariantPerformace: creation time " << t.elapsed() << " msec" << std::endl;

    t.restart();

    for (auto &variant : container) {
        if (const auto numberValue = mpark::get_if<NumberValue>(&variant)) {
            ++numberValue->data;
        } else if (const auto dateTimeValue = mpark::get_if<DateTimeValue>(&variant)) {
            ++dateTimeValue->data;
        }
    }

    std::cout << "testMparkVariantPerformace: (1th way) convert time " << t.elapsed() << " msec" << std::endl;
    std::cout << "testMparkVariantPerformace: newCounter = " << newCounter << std::endl;

    t.restart();

    for (auto &variant : container) {

        mpark::visit([](auto &&arg) {
            using T = std::decay_t<decltype(arg)>;

            static_if<std::is_same<NumberValue, T>::value>([&](auto &&f) {
                ++f(arg).data;
            });

            static_if<std::is_same<DateTimeValue, T>::value>([&](auto &&f) {
                ++f(arg).data;
            });

        }, variant);
    }

    std::cout << "testMparkVariantPerformace: (2th way) convert time " << t.elapsed() << " msec" << std::endl;
    std::cout << "testMparkVariantPerformace: newCounter = " << newCounter << std::endl;
}

void testQVariantPerformace()
{
    std::cout << "\nvoid testQVariantPerformace()\n";

    QElapsedTimer t; t.start();

    newCounter = 0;

    std::vector<QVariant> container; container.reserve(ELEMENTS_COUNT);

    for (qint64 i=0; i<ELEMENTS_COUNT; ++i) {
        if (i % 2) {
            container.push_back(QVariant::fromValue(DateTimeValue(111)));
        } else {
            container.push_back(QVariant::fromValue(NumberValue(100)));
        }
    }

    //! to measure app size
//    int wait;
//    std::cin >> wait;

    std::cout << "testQVariantPerformace: creation time " << t.elapsed() << " msec" << std::endl;

    t.restart();

    for (auto &variant : container) {
        if (variant.canConvert<NumberValue>()) {
            auto numberValue = variant.value<NumberValue>();
            ++numberValue.data;
        } else if (variant.canConvert<DateTimeValue>()) {
            auto dateTimeValue = variant.value<DateTimeValue>();
            ++dateTimeValue.data;
        }
    }

    std::cout << "testQVariantPerformace: convert time " << t.elapsed() << " msec" << std::endl;
    std::cout << "testQVariantPerformace: newCounter = " << newCounter << std::endl;
}

void testNoVariantPerformace()
{
    std::cout << "\nvoid testNoVariantPerformace()\n";

    QElapsedTimer t; t.start();

    newCounter = 0;

    std::vector<DateTimeValue> dateContainer; dateContainer.reserve(ELEMENTS_COUNT/2);
    std::vector<NumberValue> numberContainer; numberContainer.reserve(ELEMENTS_COUNT/2);

    for (qint64 i=0; i<ELEMENTS_COUNT; ++i) {
        if (i % 2) {
            dateContainer.push_back(DateTimeValue(111));
        } else {
            numberContainer.push_back(NumberValue(100));
        }
    }

    //! to measure app size
//    int wait;
//    std::cin >> wait;

    std::cout << "testQVariantPerformace: creation time " << t.elapsed() << " msec" << std::endl;
}

int main()
{
    testQVariant();
    testMparkVariant();

#ifdef QT_NO_DEBUG
    //! different results for Release Ð¸ Debug; should use with Release
    testNoVariantPerformace();      //10000000 elements - 187284 bytes virt memory
    testMparkVariantPerformace();   //10000000 elements - 304468 bytes virt memory
    testQVariantPerformace();       //10000000 elements - 617064 bytes virt memory
#endif
}
