#ifndef VALUETYPES_H
#define VALUETYPES_H

#include <QString>
#include <mpark/variant.hpp>

enum ValueType {
    //ERROR_TYPE = 0
    STRING_TYPE = 1,
    DATE_TYPE,
    NUMBER_TYPE
};

class DateTimeValue {
public:
    DateTimeValue() = default;
    DateTimeValue(const qint32 secs): data(secs) {}

    qint32 data = 0;
    bool isTime = false;
    bool isMissedValue = true;

    constexpr static ValueType type = DATE_TYPE;
};

class StringValue {
public:
    StringValue() = default;
    StringValue(const QString &string): data(string) {}

    QString data;

    constexpr static ValueType type = STRING_TYPE;
};

class NumberValue {
public:
    NumberValue() = default;
    NumberValue(const double &value) : data(value) {}

    double data = 0;
    bool isMissedValue = true;

    constexpr static ValueType type = NUMBER_TYPE;
};

class ErrorType {
};

using ValuesVariant = mpark::variant<ErrorType, StringValue, DateTimeValue, NumberValue>;

#endif // VALUETYPES_H
